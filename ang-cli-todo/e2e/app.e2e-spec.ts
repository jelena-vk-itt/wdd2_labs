import { AngCliTodoPage } from './app.po';

describe('ang-cli-todo App', function() {
  let page: AngCliTodoPage;

  beforeEach(() => {
    page = new AngCliTodoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
