import { AngCliLabPage } from './app.po';

describe('ang-cli-lab App', function() {
  let page: AngCliLabPage;

  beforeEach(() => {
    page = new AngCliLabPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
