import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { SubcompTestComponent } from './subcomp-test/subcomp-test.component';
import { ClickMeComponent } from './click-me/click-me.component';
import { ShowMsgComponent } from './show-msg/show-msg.component';

@NgModule({
  declarations: [
    AppComponent,
    SubcompTestComponent,
    ClickMeComponent,
    ShowMsgComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
