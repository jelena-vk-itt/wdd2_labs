import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-subcomp-test',
  templateUrl: './subcomp-test.component.html',
  styleUrls: ['./subcomp-test.component.css']
})
export class SubcompTestComponent implements OnInit {

  msg: string;

  constructor() { 
    this.msg = "New sub-component."
  }

  ngOnInit() {
  }

}
