import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-click-me',
  templateUrl: './click-me.component.html',
  styleUrls: ['./click-me.component.css']
})
export class ClickMeComponent implements OnInit {

  clickMsg: String;
  
  constructor() { }

  onClickMe() {
    var http = new XMLHttpRequest();
    http.open('GET', "http://5832da9cc4ca76120076b1ff.mockapi.io/todos", false);
    http.send();
    this.clickMsg = http.response;
  }

  ngOnInit() {
  }
}
